film 1
------
naam: Star Wars - A New Hope
beschrijving: Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a Wookiee and two droids to save the galaxy from the Empire's world-destroying battle-station while also attempting to rescue Princess Leia from the evil Darth Vader.

film 2
------
naam: Solaris
beschrijving: Kris Kelvin joins the space station orbiting the planet Solaris, only to find its two crew members plagued by "phantoms," creations of Solaris. Kelvin is soon confronted with his own phantom, taking the shape of his dead wife Hari.

film 3
------
naam: 2001, A Space Odyssee
beschrijving: Humanity finds a mysterious, obviously artificial object buried beneath the Lunar surface and, with the intelligent computer H.A.L. 9000, sets off on a quest.
